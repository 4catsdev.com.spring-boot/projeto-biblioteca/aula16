package com.fourcatsdev.aula16.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula16.modelo.Papel;

public interface PapelRepository extends JpaRepository<Papel, Long> {
	Papel findByPapel(String papel);
}
